/**
 * Created by xesam[416249980@qq.com] on 2014/11/6.
 */

var demo_map = new BMap.Map("container");
demo_map.centerAndZoom(new BMap.Point(116.404, 40.3), 9);
demo_map.addControl(new BMap.NavigationControl());
demo_map.addControl(new BMap.ScaleControl());
demo_map.addControl(new BMap.OverviewMapControl());
demo_map.enableScrollWheelZoom();
demo_map.addControl(new BMap.MapTypeControl());


function LayerA() {
    Xe.OverlayLayer.apply(this, arguments);
}
Xe.extend(LayerA, Xe.MarkerLayer);


function LayerB() {
    Xe.OverlayLayer.apply(this, arguments);
}
Xe.extend(LayerB, Xe.MarkerLayer);

function AdapterA(data) {
    Xe.OverlayLayerAdapter.apply(this, arguments);
}
Xe.extend(AdapterA, Xe.OverlayLayerAdapter);
AdapterA.prototype.getOverlay = function (index) {
    var overlay = new BMap.Circle(this.getPoint(index), 2000, {
        strokeColor: 'green',
        fillColor: 'green',
        strokeWeight: 2,
        strokeOpacity: 0.6,
        fillOpacity: 0.6
    });
    return overlay;
};

function AdapterB(data) {
    Xe.OverlayLayerAdapter.apply(this, arguments);
}
Xe.extend(AdapterB, Xe.OverlayLayerAdapter);
AdapterB.prototype.getOverlay = function (index) {
    var marker = AdapterB.superclass.getOverlay.call(this, index);
    marker.enableDragging();
    return marker;
};

function mock_ajax(fn) {
    setTimeout(fn, 2000);
}

function $(id) {
    return document.getElementById(id);
}

mock_ajax(function () {
    var layerAProvider = (function () {
        var i = 0;
        var data = [];
        while (i++ < 10) {
            data.push({
                'lng': 116.404 + 0.1 * i,
                'lat': 39.915
            });
        }
        return data;
    })();
    var adapterA = new AdapterA(layerAProvider);
    var layerA = new LayerA();
    layerA.setAdapter(adapterA).setEventListner('click', function (event) {
        console.log('单击layerA:' + event.target.index);
    }).setEventListner('dblclick', function (event) {
        console.log('双击layerA:' + event.target.index);
    });
    layerA.addToMap(demo_map);
    $('demo_btn_clear_a').addEventListener('click', function (event) {
        layerA.removeFromMap(demo_map);
    }, false);
});

mock_ajax(function () {
    var layerBProvider = (function () {
        var i = 0;
        var data = [];
        while (i++ < 10) {
            data.push({
                'lng': 115.404,
                'lat': 39.915 + 0.1 * i
            });
        }
        return data;
    })();
    var adapterB = new AdapterB(layerBProvider);
    var layerB = new LayerB();
    layerB.setAdapter(adapterB).click(function (event) {
        console.log('单击layerB:' + event.target.index);
    }).setEventListner('dragend', function (event) {
        console.log('dragend layerB:' + event.target.index);
    });
    layerB.addToMap(demo_map).renderPoly(demo_map);
    $('demo_btn_clear_b').addEventListener('click', function (event) {
        layerB.removeFromMap(demo_map);
    }, false);
});