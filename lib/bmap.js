/**
 * Created by Xesam[416249980@qq.com] on 14-11-5.
 */

if (window.Xe) {
    throw "namespace[Xe] confilct";
} else {
    window.Xe = {}
}

Xe.extend = function (Child, Parent) {
    var F = function () {
    };
    F.prototype = Parent.prototype;
    Child.prototype = new F();
    Child.prototype.constructor = Child;
    Child.superclass = Parent.prototype;
};

(function () {
    function OverlayLayer() {
        this.listners = {};
        this.markers = [];
    }

    OverlayLayer.prototype = {
        constructor: OverlayLayer,
        'inflate': function () {
            for (var i = 0, size = this.adapter.getCount(); i < size; i++) {
                var marker = this.adapter.getOverlay(i);
                marker.index = i;
                this.markers.push(marker);
            }
        },
        'renderPoly': function (bmap) {
            var points = [];
            for (var i = 0, size = this.markers.length; i < size; i++) {
                var marker = this.markers[i];
                points.push(marker.getPosition());
            }
            var polyline = new BMap.Polyline(points, {
                strokeColor: 'green',
                fillColor: 'green',
                strokeWeight: 2,
                strokeOpacity: 0.6,
                fillOpacity: 0.6
            })
            this.polyline = polyline;
            bmap.addOverlay(polyline);
        },
        'addToMap': function (bmap) {
            this.inflate();
            for (var i = 0, size = this.markers.length; i < size; i++) {
                var marker = this.markers[i];
                for (var eventName in this.listners) {
                    marker.addEventListener(eventName, this.listners[eventName]);
                }
                bmap.addOverlay(marker);
            }
            return this;
        },
        'removeFromMap': function (bmap) {
            for (var i = 0, size = this.markers.length; i < size; i++) {
                var marker = this.markers[i];
                for (var eventName in this.listners) {
                    marker.removeEventListener(this.listners[eventName]);
                }
                bmap.removeOverlay(marker);
            }
            this.markers = [];
        },
        'setAdapter': function (adapter) {
            this.adapter = adapter;
            return this;
        },
        'click': function (l) {
            this.setEventListner('click', l);
            return this;
        },
        'setEventListner': function (eventName, fn) {
            if (this.listners[eventName]) {
                throw eventName + ' already exist';
            }
            this.listners[eventName] = fn;
            return this;
        }
    };

    function OverlayLayerAdapter(data) {
        this.data = data;
    }

    OverlayLayerAdapter.prototype = {
        constructor: OverlayLayerAdapter,
        'getItem': function (index) {
            return this.data[index];
        },

        'getPoint': function (index) {
            var item = this.getItem(index);
            return new BMap.Point(item['lng'], item['lat']);
        },

        'getOverlay': function (index) {
            var overlay = new BMap.Marker(this.getPoint(index));
            this.postOverlay(index, overlay);
            return overlay;
        },

        'postOverlay': function (index, overlay) {

        },

        'getCount': function () {
            return this.data.length;
        }
    };


    Xe.OverlayLayer = OverlayLayer;
    Xe.OverlayLayerAdapter = OverlayLayerAdapter;

    /**
     * 废弃
     * */
    Xe.MarkerLayer = OverlayLayer;
    /**
     * 废弃
     * */
    Xe.MarkerLayerAdapter = OverlayLayerAdapter;

})()



