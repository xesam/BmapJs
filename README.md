#BmapJs [Beta]
一个简单的简化百度地图js操作的工具类


##功能说明

将一组marker抽象为一个layer，从而可以用一个layer管理一组集合点。

虽然百度地图本身提供了TileLayer等等类，但是并没有开放更多的可用方法，所以用处基本不大。

基本结构：

    Layer <--> Adapter <--> dataProvider

##使用方法

    new Xe.OverlayLayer().setAdapter(new Xe.OverlayLayerAdapter(stationProvider)).click(function (event) {
        console.log('单击layer:' + event.target.index);
    });

##Demo

详见demo

##修改记录

1. 2014.11.12

    1. 修改拼写错误，Adatper修改为Adapter
    2. 废弃MarkerLayer，修改为OverlayLayer
    3. 修改demo的一些写法，直接使用继承语句，而不再重复父类结构

        AdapterB.superclass.getOverlay.call(this, index);


